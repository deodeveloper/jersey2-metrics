package org.mpierce.jersey2.metrics.test;

import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.mpierce.jersey2.metrics.MetricNamer;

import javax.annotation.Nullable;

public class StubNamer implements MetricNamer {

    public static final String TIMER_NAME = "timer";
    public static final String COUNTER_NAME = "statusCodes";

    private final String timerName;
    private final String counterName;

    public StubNamer() {
        this(TIMER_NAME, COUNTER_NAME);
    }

    public StubNamer(String timerName, String counterName) {
        this.timerName = timerName;
        this.counterName = counterName;
    }

    @Nullable
    @Override
    public String getTimerName(RequestEvent event) {
        return timerName;
    }

    @Nullable
    @Override
    public String getStatusCodeCounterName(RequestEvent event) {
        return counterName;
    }
}
