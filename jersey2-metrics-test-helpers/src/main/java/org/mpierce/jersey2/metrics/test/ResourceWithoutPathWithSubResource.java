package org.mpierce.jersey2.metrics.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

public class ResourceWithoutPathWithSubResource {
    @GET
    @Path("subResource")
    public String get() {
        return "get";
    }
}
