package org.mpierce.jersey2.metrics;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Timer;
import com.google.common.base.Preconditions;
import java.util.Map;
import java.util.SortedMap;
import java.util.concurrent.ExecutionException;
import javax.annotation.Nonnull;
import org.junit.Test;
import org.mpierce.jersey2.metrics.test.AbstractMetricsTestBase;

import static org.junit.Assert.assertEquals;

public final class FullStackTest extends AbstractMetricsTestBase {

    @Test
    public void testMakeRequestToBasicResource() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/resource").getStatusCode());

        waitForTimers();

        Map.Entry<String, Timer> timerEntry = registry.getTimers().entrySet().stream()
                .filter(e -> e.getValue().getCount() > 0).findFirst().get();

        assertEquals(1, timerEntry.getValue().getCount());

        Map.Entry<String, Counter> counterEntry = registry.getCounters().entrySet().stream()
                .filter(e -> e.getValue().getCount() > 0).findFirst().get();

        assertEquals(1, counterEntry.getValue().getCount());
    }

    @Test
    public void testMakeRequestToSubresource() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/resourceWithSubresource/sub").getStatusCode());
    }

    @Test
    public void testMakeRequestToSubresourceLocatorWithoutSubresource() throws ExecutionException,
            InterruptedException {

        assertEquals(200, req("/resourceWithSubResourceLocators/srlWithResource").getStatusCode());
    }

    @Test
    public void testMakeRequestToSuperclass() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/parent").getStatusCode());

        assertTimerPresent("/parent.method=GET.timer");
    }

    @Test
    public void testMakeRequestToSubclass() throws ExecutionException, InterruptedException {
        assertEquals(200, req("/child").getStatusCode());

        assertTimerPresent("/child.method=GET.timer");
    }

    private void assertTimerPresent(String name) throws InterruptedException {

        SortedMap<String, Timer> timers = waitForTimers();

        assertEquals(1, timers.size());

        Map.Entry<String, Timer> entry = timers.entrySet().iterator().next();
        assertEquals(name, entry.getKey());
        Timer timer = entry.getValue();
        assertEquals(1, timer.getCount());
    }

    /**
     * Spin for up to 10ms waiting for the event to hit the metrics listener
     *
     * @return timers map
     */
    @Nonnull
    private SortedMap<String, Timer> waitForTimers() throws InterruptedException {
        long start = System.nanoTime();

        SortedMap<String, Timer> timers = null;
        while (System.nanoTime() - start < 10_000_000) {
            timers = registry.getTimers();
            if (!timers.isEmpty()) {
                break;
            }

            Thread.sleep(1);
        }
        Preconditions.checkNotNull(timers);

        return timers;
    }

    @Override
    protected MetricArbiter getMetricArbiter() {
        return new FixedMetricArbiter(true, true);
    }

    @Override
    protected MetricNamer getMetricNamer() {
        return new RequestPathMetricNamer();
    }
}
