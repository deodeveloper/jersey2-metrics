package org.mpierce.jersey2.metrics;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.UniformReservoir;
import org.glassfish.jersey.server.internal.monitoring.RequestEventImpl;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.junit.Before;
import org.junit.Test;
import org.mpierce.jersey2.metrics.test.StubNamer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MetricsRequestEventListenerTest {

    MetricRegistry registry;
    private MetricProvider metricProvider;
    private MetricNamer metricNamer;

    @Before
    public void setUp() throws Exception {
        registry = new MetricRegistry();
        metricProvider = new MetricProvider(registry, UniformReservoir::new);
        metricNamer = new StubNamer();
    }

    @Test
    public void testDoesntTimeIfNotRequested() {
        MetricsRequestEventListener listener =
            new MetricsRequestEventListener(new FixedMetricArbiter(false, false), metricProvider, metricNamer);

        listener.onEvent(new RequestEventImpl.Builder().build(RequestEvent.Type.FINISHED));

        assertTrue(registry.getTimers().isEmpty());
    }

    @Test
    public void testDoesntCountStatusCodeIfNotRequested() {
        MetricsRequestEventListener listener =
            new MetricsRequestEventListener(new FixedMetricArbiter(false, false), metricProvider, metricNamer);

        listener.onEvent(new RequestEventImpl.Builder().build(RequestEvent.Type.FINISHED));

        assertTrue(registry.getCounters().isEmpty());
    }

    @Test
    public void testTimesRequest() {
        MetricsRequestEventListener listener =
            new MetricsRequestEventListener(new FixedMetricArbiter(true, true), metricProvider, metricNamer);

        listener.onEvent(new RequestEventImpl.Builder().build(RequestEvent.Type.FINISHED));

        assertFalse(registry.getTimers().isEmpty());
        assertEquals(1, registry.timer(StubNamer.TIMER_NAME).getCount());
    }

    @Test
    public void testDoesntTimeIfRequestedButCannotGetName() {
        MetricsRequestEventListener listener =
            new MetricsRequestEventListener(new FixedMetricArbiter(true, true), metricProvider,
                new StubNamer(null, null));

        listener.onEvent(new RequestEventImpl.Builder().build(RequestEvent.Type.FINISHED));

        assertTrue(registry.getTimers().isEmpty());
    }

    @Test
    public void testDoesntCountStatusCodeIfRequestedButCannotGetName() {
        MetricsRequestEventListener listener =
            new MetricsRequestEventListener(new FixedMetricArbiter(true, true), metricProvider,
                new StubNamer(null, null));

        listener.onEvent(new RequestEventImpl.Builder().build(RequestEvent.Type.FINISHED));

        assertTrue(registry.getCounters().isEmpty());
    }

    @Test
    public void testCountsStatusCode() {
        MetricsRequestEventListener listener =
            new MetricsRequestEventListener(new FixedMetricArbiter(true, true), metricProvider, metricNamer);

        listener.onEvent(new RequestEventImpl.Builder().build(RequestEvent.Type.FINISHED));

        assertFalse(registry.getCounters().isEmpty());
        assertEquals(1, registry.counter(StubNamer.COUNTER_NAME).getCount());
    }
}
