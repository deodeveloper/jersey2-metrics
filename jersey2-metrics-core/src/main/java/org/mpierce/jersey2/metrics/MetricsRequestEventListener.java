package org.mpierce.jersey2.metrics;

import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;

import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.TimeUnit;

@ThreadSafe
final class MetricsRequestEventListener implements RequestEventListener {

    private final MetricArbiter metricArbiter;

    private final MetricProvider metricProvider;
    private final MetricNamer metricNamer;

    /**
     * Record start time in case we end up wanting to have a timer. We have to calculate the start time here, and rely
     * on this specific type of clock being used in the Timer, because the RequestEvent we have at construction time is
     * of type START, which doesn't have ExtendedUriInfo, which is what we would need to do anything useful for a
     * MetricArbiter/MetricNamer to do anything useful to be able to have the Timer here.
     */
    private final long startTime = System.nanoTime();

    MetricsRequestEventListener(MetricArbiter metricArbiter, MetricProvider metricProvider, MetricNamer metricNamer) {
        this.metricArbiter = metricArbiter;
        this.metricProvider = metricProvider;
        this.metricNamer = metricNamer;
    }

    public void onEvent(RequestEvent event) {
        if (event.getType() == RequestEvent.Type.FINISHED) {
            if (metricArbiter.shouldHaveTimer(event)) {
                String timerName = metricNamer.getTimerName(event);
                if (timerName != null) {
                    metricProvider.getTimer(timerName).update(System.nanoTime() - startTime, TimeUnit.NANOSECONDS);
                }
            }

            if (metricArbiter.shouldHaveStatusCodeCounter(event)) {
                String statusCodeCounterName = metricNamer.getStatusCodeCounterName(event);
                if (statusCodeCounterName != null) {
                    metricProvider.getStatusCodeCounter(statusCodeCounterName).inc();
                }
            }
        }
    }
}
