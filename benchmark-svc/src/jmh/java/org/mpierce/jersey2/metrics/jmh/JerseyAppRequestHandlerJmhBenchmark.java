package org.mpierce.jersey2.metrics.jmh;

import com.codahale.metrics.MetricRegistry;
import org.glassfish.jersey.server.ApplicationHandler;
import org.glassfish.jersey.server.ContainerResponse;
import org.glassfish.jersey.test.util.server.ContainerRequestBuilder;
import org.mpierce.jersey2.metrics.FixedMetricArbiter;
import org.mpierce.jersey2.metrics.MetricsAppEventListener;
import org.mpierce.jersey2.metrics.benchmark.BenchmarkApp;
import org.mpierce.jersey2.metrics.benchmark.BenchmarkSvc;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.GroupThreads;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

@BenchmarkMode(Mode.Throughput)
public class JerseyAppRequestHandlerJmhBenchmark {

    @State(Scope.Benchmark)
    public static class BenchmarkState {

        String path = "staticString";

        @Param
        Mode mode;

        ApplicationHandler handler;

        @Setup(Level.Trial)
        public void setUp() {

            BenchmarkApp app;
            MetricRegistry registry = new MetricRegistry();

            switch (mode) {
                case WITH_DEFAULT_METRICS:
                    app = new BenchmarkApp(new MetricsAppEventListener.Builder(registry)
                        .build());
                    break;
                case NO_METRICS:
                    app = new BenchmarkApp();
                    break;
                case DISABLED_METRICS:
                    app = new BenchmarkApp(
                        new MetricsAppEventListener.Builder(registry)
                            .withArbiter(new FixedMetricArbiter(false, false))
                            .build());
                    break;
                case STUB_NAMER:
                    app = new BenchmarkApp(
                        new MetricsAppEventListener.Builder(registry)
                            .withNamer(new BenchmarkSvc.StubNamer())
                            .build());
                    break;
                default:
                    throw new IllegalStateException("Unknown mode " + mode);
            }

            handler = new ApplicationHandler(app);
        }
    }

    public enum Mode {
        STUB_NAMER,
        DISABLED_METRICS,
        WITH_DEFAULT_METRICS,
        NO_METRICS
    }

    @Benchmark
    @GroupThreads(2)
    public ContainerResponse measure(BenchmarkState benchmarkState) throws Exception {

        return benchmarkState.handler.apply(ContainerRequestBuilder
            .from(benchmarkState.path, "GET")
            .build())
            .get();
    }
}
