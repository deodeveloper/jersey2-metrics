package org.mpierce.jersey2.metrics.benchmark;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/staticString")
public final class StaticStringResource {

    @GET
    public String get() {
        return "get";
    }

}
