package org.mpierce.jersey2.metrics.benchmark;

import com.codahale.metrics.MetricRegistry;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.servlet.ServletContainer;
import org.mpierce.jersey2.metrics.MetricNamer;
import org.mpierce.jersey2.metrics.MetricsAppEventListener;
import org.slf4j.bridge.SLF4JBridgeHandler;

import javax.annotation.Nullable;
import javax.servlet.Servlet;
import java.util.logging.LogManager;

public final class BenchmarkSvc {

    private static final int PORT = 9090;

    public static void main(String[] args) throws Exception {
        LogManager.getLogManager().reset();
        SLF4JBridgeHandler.install();

        MetricRegistry registry = new MetricRegistry();

        MetricNamer namer = new StubNamer();

        MetricsAppEventListener listener = new MetricsAppEventListener.Builder(registry)
            .withNamer(namer)
            .build();
        BenchmarkApp appWithMetrics = new BenchmarkApp(listener);

        Server server = getServer(new ServletContainer(appWithMetrics));
        server.start();
    }

    private static Server getServer(Servlet servlet) {
        Server server = new Server(PORT);
        ServletContextHandler servletHandler = new ServletContextHandler();

        servletHandler.addServlet(new ServletHolder(servlet), "/*");

        server.setHandler(servletHandler);

        return server;
    }

    public static class StubNamer implements MetricNamer {
        @Nullable
        @Override
        public String getTimerName(RequestEvent event) {
            return "timer";
        }

        @Nullable
        @Override
        public String getStatusCodeCounterName(RequestEvent event) {
            return "counter";
        }
    }
}
